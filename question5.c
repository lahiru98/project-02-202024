#include <stdio.h>
int main()
{
    int a=10, b=15;
    printf("    A=%d, B=%d\n", a,b);

    printf("a.  A&B: %d\n", a&b);
    printf("b.  A^B: %d\n", a^b);
    printf("c.   ~A: %d\n", ~a);
    printf("d. A<<3: %d\n", a<<3);
    printf("e. B>>3: %d", b>>3);
    return 0;
}
