#include <stdio.h>
int main()
{
    int num1, num2, num3, sum;
    float avg;

    printf(" Input first number: ");
    scanf("%d", &num1);
    printf(" Input second number: ");
    scanf("%d", &num2);
    printf(" Input third number: ");
    scanf("%d", &num3);

    sum = num1 + num2 + num3;
    avg = (float)(num1+num2+num3)/3;

    printf(" \nSum = %d", sum);
    printf(" \nAverage = %.2f", avg);

return 0;
}
