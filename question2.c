#include <stdio.h>
int main()
{
    float radius, height, volume;
    printf( " Input radius of the cone: ");
    scanf("%f", &radius);
    printf(" Input height of the cone: ");
    scanf("%f", &height);

    volume = (22*radius*radius*height)/(3*7);
    printf(" Volume of the cone: %.2f\n", volume);
    return 0;
}
