#include <stdio.h>
int main()
{
    int num1, num2;
    printf("Input first number: ");
    scanf("%d", &num1);
    printf("Input second number: ");
    scanf("%d", &num2);

    num1=num1+num2;
    num2=num1-num2;
    num1=num1-num2;

    printf("After swapping,\n   First number  = %d\n   Second number = %d", num1, num2);
    return 0;
}
